# PHP-FPM 7.x

FROM php:7-fpm

`php -m`
```
[PHP Modules]
bcmath, Core, ctype, curl, date, dom, fileinfo, filter, ftp, gd, gettext, hash, iconv, intl, json, libxml, mbstring, mcrypt, mongodb, mysqlnd, openssl, pcre, PDO, pdo_mysql, pdo_sqlite, Phar, posix, readline, Reflection, session, SimpleXML, SPL, sqlite3, standard, tokenizer, xml, xmlreader, xmlwriter, Zend OPcache, zip, zlib
[Zend Modules]
Zend OPcache
```
PHP version 7.3


## features
+ works as Your local user (on Linux, Mac, Windows)
* full i18n support (locale, intl, gettext)
* able to run composer
* Symfony & Yii frameworks compatibility test passed


## using - quick HOWTO
1) Create `docker-compose.yml` in Your project

2) Add `fpm` rule as following:
```
fpm:
    image: amsdard/php:7
    container_name: php7-fpm
    volumes:
        - ~/YourProject:/opt
    restart: always
```

* set up `/opt` docker's location as main project directory

3) Create "Yakefile" file in Your base project's path
```
php: docker-compose exec php entrypoint php $CMD
```


## run

```
docker-compose up -d
yake php /opt/check.php 
yake php -m
```

## Install new locale
Add to `Dockerfile`
```
RUN echo "pl_PL.UTF-8 UTF-8" >> /etc/locale.gen && \
    locale-gen --purge
```

## ImageMagick support

Create own Dockerfile using below schema:
```
FROM amsdard/php:7

RUN curl -fsSL 'http://www.imagemagick.org/download/ImageMagick.tar.gz' -o ImageMagick.tar.gz && \
    tar xvzf ImageMagick.tar.gz && \
	cd ImageMagick-* && ./configure && yake && yake install && /sbin/ldconfig /usr/local/lib && \
	cd .. && rm -rf ImageMagick.tar.gz ImageMagick-* && \
	POLICY_XML_LOCATION="$(find /usr/local/etc/ -name 'policy.xml')" && \
	cp /files/imagemagick-policy.xml $POLICY_XML_LOCATION
	
RUN pecl install imagick && \
    docker-php-ext-enable imagick
```


## advanced using - composer
To run composer using local `composer.phar` file and docker's PHP, You should use the following `docker-compose.yml` content:
```
php7:
  [...]
  volumes:
    - ~/.docker-composer:/var/www/.composer
    - ~/.ssh:/var/www/.ssh
    [...]
```
* `/var/www/` is home directory of "www-data" user. `.composer` is cache directory, and `.ssh` allows composer to use Your permissions.

And add to `Yakefile` (above the last line):
```
[...]
composer: $BIN php ./composer.phar --optimize-autoloader $CMD
[...]
```

then run:
`yake composer install`
`yake composer require packagename`
`yake composer --help`
